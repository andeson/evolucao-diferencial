package br.ufs.dcom.buscaeotimizacao;

import br.ufs.dcom.buscaeotimizacao.ed.EvolucaoDiferencial;
import br.ufs.dcom.buscaeotimizacao.funcoes.*;
import br.ufs.dcom.buscaeotimizacao.utils.Utils;

public class Main {

    public static String path;

    public static void main(String args[]){
        path = "/tmp/ED/testes/";
        final int MAX_TESTS = 10;
        long begin, end;
        Functions f;
        int[] population_size_test = {100, 200, 500};
        int[] max_generation_tests = {2000};

        for (int i = 0; i < 4; i++) {
            if (i == 0) {
                f = new One();
                System.out.println("Criando diretorio" + f.name);
                Utils.createDirectory(path + f.name);
            } else {
                if (i == 1) {
                    f = new Three();
                    System.out.println("Criando diretorio" + f.name);
                    Utils.createDirectory(path + f.name);
                } else {
                    if (i == 2) {
                        f = new Nine();
                        System.out.println("Criando diretorio" + f.name);
                        Utils.createDirectory(path + f.name);
                    } else {
                        f = new Eleven();
                        System.out.println("Criando diretorio" + f.name);
                        Utils.createDirectory(path + f.name);
                    }
                }
            }
            for (int k = 0; k < max_generation_tests.length; k++) {
                for (int j = 0; j < population_size_test.length; j++) {
                    System.out.println("Executando " + f.name + " Tamanho enxame " + population_size_test[j]);
                    EvolucaoDiferencial ed = new EvolucaoDiferencial(f, population_size_test[j], max_generation_tests[k]);
                    begin = System.currentTimeMillis();
                    for (int l = 0; l < MAX_TESTS; l++) {
                        double r = ed.execute();
                        r = Utils.checkPrecision(r, f);
                        Utils.writeResultTestes(r, population_size_test[j], max_generation_tests[k],f.name);
                    }
                    end = System.currentTimeMillis();
                    Utils.writeTime((end - begin), f.name, population_size_test[j], max_generation_tests[k]);
                }
            }
        }
    }
}
