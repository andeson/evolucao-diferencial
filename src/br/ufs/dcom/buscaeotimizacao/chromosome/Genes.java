package br.ufs.dcom.buscaeotimizacao.chromosome;

public class Genes {

    private double[] genes;

    public Genes(double[] genes) {
        super();
        this.genes = genes;
    }

    public double[] getGenes() {
        return genes;
    }

    public void setGenes(double[] genes) {
        this.genes = genes;
    }

}
