package br.ufs.dcom.buscaeotimizacao.chromosome;

import br.ufs.dcom.buscaeotimizacao.funcoes.Functions;

public class Chromosome {

    private double fitness;
    private Genes genes;

    public Chromosome() {
        super();
    }

    public Chromosome(double fitness, Genes location) {
        this.fitness = fitness;
        this.genes = location;
    }

    public Genes getGenes() {
        return genes;
    }

    public void setGenes(Genes genes) {
        this.genes = genes;
    }

    public double getFitness(Functions f) {
        fitness = f.evaluate(genes);
        return fitness;
    }

}
