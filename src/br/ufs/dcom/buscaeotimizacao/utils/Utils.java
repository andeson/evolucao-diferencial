package br.ufs.dcom.buscaeotimizacao.utils;

import br.ufs.dcom.buscaeotimizacao.Main;
import br.ufs.dcom.buscaeotimizacao.funcoes.Functions;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utils {
    public static void writeResultTestes(double r, int size_pop, int generations, String name_function) {
        File file = new File(Main.path + name_function + "/Solucao_Testes_Tamanho_enxame " + size_pop + ".txt");
        try {
            try (Writer bw = new BufferedWriter(new FileWriter(file, true))) {
                bw.write(String.valueOf(r));
                bw.write("\n");
                bw.flush();
                bw.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static double checkPrecision(double r, Functions f) {
        if ((r - f.optimum) < Functions.PRECISION) {
            return 0.0;
        } else {
            return r;
        }
    }

    public static void writeConvergence(double convergence, int size_pop, int generations, String name_function) {
        File file = new File(Main.path + name_function + "/Solucao_cada_geracao-" + size_pop + ".txt");
        try (Writer bw = new BufferedWriter(new FileWriter(file, true));) {
            bw.append(String.valueOf(convergence));
            bw.append("\n");
            bw.flush();
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void writeTime(long time, String name_function, int size_pop, int generations) {
        File file = new File(Main.path + name_function + "/Tempo_execucao_Tamanho_enxame-" + size_pop + ".txt");
        try (Writer bw = new BufferedWriter(new FileWriter(file, true));) {
            bw.write(String.valueOf(time));
            bw.write("\n");
            bw.flush();
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void createDirectory(String directory) {
        try {
            if (!new File(directory + "/").exists()) {
                (new File(directory + "/")).mkdir();
            }
        } catch (Exception ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
