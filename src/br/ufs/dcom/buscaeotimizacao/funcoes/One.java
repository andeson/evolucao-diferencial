package br.ufs.dcom.buscaeotimizacao.funcoes;

import br.ufs.dcom.buscaeotimizacao.chromosome.Genes;

public class One extends Functions {
    public One() {
        X_HIGH = 100;
        X_LOW = -100;
        name = "Funcao_1";
        optimum = 0;
        CR = 0.2;
    }

    @Override
    public double evaluate(Genes genes) {
        double loc[] = genes.getGenes();
        double fitness = 0;
        for (int i =0; i<loc.length; i++){
            fitness+= Math.pow(loc[i],2);
        }
        return fitness;
    }
}
